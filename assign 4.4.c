#include <stdio.h>
int main() {
    int numb, i;
    printf("Enter any integer: ");
    scanf("%d", &numb);
    printf("\nThe factors of %d are: ", numb);
    for (i = 1; i <= numb; ++i) {
        if (numb % i == 0) {
            printf("%d ", i);
        }
    }
    printf("\n");
    return 0;
}
