#include <stdio.h>
int main() {
    int number, reverse = 0, rem;
    printf("Enter any number: ");
    scanf("%d", &number);
    while (number != 0) {
        rem = number % 10;
        reverse = reverse * 10 + rem;
        number /= 10;
    }
    printf("Reversed number is: %d", reverse);
    return 0;
}
